<?php

use Illuminate\Support\Facades\Route;

Route::match(['get', 'post'],'/', function () {
    $request = request();
    if ($request->isMethod('POST')) {
        request()->validate([
            'name' => 'cnpj'
        ]);
        return 'ok';
    }
    return view('welcome');
});

Route::group([
    'prefix' => 'admin/tags',
    'as' => 'tags.',
    'namespace' => 'Admin',
], function () {
    Route::get('/{tag?}', 'TagsController@index')->name('index');
    Route::post('/store', 'TagsController@store')->name('store');
    Route::put('/update/{tag}', 'TagsController@update')->name('update');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
