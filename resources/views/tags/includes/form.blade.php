<form method="post" action="{{ !empty($tag) ? route('tags.update', $tag) :route('tags.store') }}" enctype="multipart/form-data">
    @csrf
    @if(!empty($tag))
    @method('put')
    @endif

    <div class="card text-left">
        <h3 class="card-header">
            @if(!empty($tag))
            Editando Tag: <strong>{{ $tag->name }}</strong>
            @else
            Criar Tag
            @endif
        </h3>
        <div class="card-body">
            <div class="form-group {{ $errors->has('name') ? 'has-error' : null }}">
                <label>
                    <small class="text-danger">*</small>
                    Nome:
                </label>
                <input type="text" name="name" value="{{ old('name') ?? $tag->name ?? null }}" class="form-control" placeholder="Informe o nome">
                @if($errors->has('name'))
                <div class="help-block">
                    {{ $errors->first('name') }}
                </div>
                @endif
            </div>
            <div class="form-group {{ $errors->has('metadata.description') ? 'has-error' : null }}">
                <label>
                    <small class="text-danger">*</small>
                    Descrição:
                </label>
                <textarea maxlenght="155" name="metadata[description]" value="{{ old('description') ?? $tag->metadata->description ?? null }}" class="form-control">{{ old('metadata.description', $tag->metadata->description ?? null) }}</textarea>
                @if($errors->has('metadata.description'))
                <div class="help-block">
                    {{ $errors->first('metadata.description') }}
                </div>
                @endif
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('metadata.background') ? 'has-error' : null }}">
                        <label>
                            Cor de fundo:
                        </label>
                        <input type="color" name="metadata[background]" value="{{ old('metadata.background') ?? $tag->metadata->background ?? '#000' }}" class="form-control">
                        @if($errors->has('metadata.background'))
                        <div class="help-block">
                            {{ $errors->first('metadata.background') }}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('metadata.font_color') ? 'has-error' : null }}">
                        <label>
                            Cor da fonte:
                        </label>
                        <input type="color" name="metadata[font_color]" value="{{ old('metadata.font_color') ?? $tag->metadata->font_color ?? '#fff' }}" class="form-control">
                        @if($errors->has('metadata.font_color'))
                        <div class="help-block">
                            {{ $errors->first('metadata.font_color') }}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group {{ $errors->has('type') ? 'has-error' : null }}">
                <label>
                    <small class="text-danger">*</small>
                    Tipo:
                </label>
                <select class="form-control" name="type">
                    <option {{old('type') == 'global' || !empty($tag) && $tag->type == 'global' ? 'selected' : null}} value="global">Global</option>
                    <option {{old('type') == 'category' || !empty($tag) && $tag->type == 'category' ? 'selected' : null}} value="category">Category</option>
                </select>
                @if($errors->has('type'))
                <div class="help-block">
                    {{ $errors->first('type') }}
                </div>
                @endif
            </div>

            @if(!empty($tag))
            <button class="btn btn-primary float-left">
                <i class="far fa-save"></i>
                Atualizar
            </button>
            @else
            <button class="btn btn-success float-left">
                <i class="far fa-save"></i>
                Criar
            </button>
            @endif
        </div>
    </div>
</form>