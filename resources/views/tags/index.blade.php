@extends('layouts.admin')

@section('content')
<div class="row">
    <div class="col-md-4 col-12">
        @include('tags.includes.form')
    </div>
    <div class="col-md-8 col-12">
        <div class="card">
                <h3 class="card-header">Lista de Tags</h3>
            <div class="card-body">
                @include('tags.includes.table')
            </div>
        </div>
    </div>
</div>
@endsection
