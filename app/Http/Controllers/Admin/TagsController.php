<?php

namespace App\Http\Controllers\Admin;

use Rockbuzz\LaraTags\Models\Tag;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TagRequest;

class TagsController extends Controller
{
    private $tag;

    public function __construct(Tag $tag)
    {
        $this->tag = $tag;
    }

    public function index(Tag $tag = null)
    {
        $tags = $this->tag->latest()->get();

        if (request()->expectsJson()) {
            return $tags;
        }

        return view('tags.index', compact('tags', 'tag'));
    }

    public function store(TagRequest $request)
    {
        $tag = $this->tag->create($request->validated());

        if (request()->expectsJson()) {
            return $tag;
        }

        return redirect()->route('tags.index')
            ->withSuccess('Tag criada com sucesso!');
    }

    public function update(Tag $tag, TagRequest $request)
    {
        $tag = $tag->update($request->validated());

        if (request()->expectsJson()) {
            return $tag;
        }

        return redirect()->route('tags.index')
            ->withSuccess('Tag atualizada com sucesso!');
    }
}
