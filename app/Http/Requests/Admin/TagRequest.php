<?php

namespace App\Http\Requests\Admin;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class TagRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $tag = $this->route('tag');

        $type = $this->get('type');

        $unique = Rule::unique('tags', 'name')->where(function ($query) use ($tag, $type) {
            return $query->where('type', $type)
                ->where('id', '!=', $tag->id ?? null);
        });

        return [
            'name' => array('required', 'string', 'max:255', $unique),
            'type' => 'required',
            'metadata.description' => 'required|string|max:155',
            'metadata.background' => 'string',
            'metadata.font_color' => 'string',
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'título',
            'type' => 'tipo',
            'metadata.description' => 'descrição',
            'metadata.background' => 'cor de fundo',
            'metadata.font_color' => 'cor da fonte'
        ];
    }
}
