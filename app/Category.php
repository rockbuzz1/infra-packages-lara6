<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rockbuzz\LaraActivities\Traits\RecordsActivity;

class Category extends Model
{
    use RecordsActivity;

    protected $fillable = ['name', 'metadata'];

    protected $casts = ['metadata' => 'array'];
}
