<?php

use Illuminate\Database\Seeder;
use Rockbuzz\LaraTags\Models\Tag;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $tags = factory(Tag::class, 15)->create();
    }
}
