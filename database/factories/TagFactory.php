<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Faker\Generator as Faker;
use Rockbuzz\LaraTags\Models\Tag;

$factory->define(Tag::class, function (Faker $faker) {
    $name = $faker->unique()->word;
    return [
        'name' => $name,
        'slug' => Str::slug($name),
        'type' => Arr::random(['global', 'category']),
        'metadata' => [
            'description' => $faker->sentence,
            'background' => $faker->hexColor,
            'font_color' => $faker->hexColor
        ]
    ];
});
