<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Category;

$factory->define(Category::class, function (Faker $faker) {
    $name = $faker->unique()->word;
    return [
        'name' => $name,
        'metadata' => json_encode([
            'description' => $faker->sentence,
            'background' => $faker->hexColor,
            'font_color' => $faker->hexColor
        ])
    ];
});
